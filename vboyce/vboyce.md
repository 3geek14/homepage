---
title: "vboyce knitting"
...

::::::: {.container-content}

::::: {.container-thumbnails}

<details>
<summary class="item-thumbnail toggle image-1">

![Green Triangle Shawl](https://vboyce.github.io/assets/green-tri-1.jpg){.thumbnail}

</summary>
::: {.item-description .description-1}
Made Fall 2017.
Pattern is Sunflower Shawl from New Vintage Lace by Andrea Jurgrau.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at elementum nibh, vel ullamcorper urna. Phasellus et malesuada orci. Proin a leo mauris. Ut ut ornare dui, nec bibendum magna. Sed quam turpis, tristique a imperdiet ut, fermentum condimentum tellus. Praesent feugiat, felis at suscipit pellentesque, felis augue convallis leo, nec porta tellus orci nec lorem. Donec facilisis, nunc ut dapibus tincidunt, urna neque volutpat sem, in cursus metus urna nec sapien. Integer egestas nec elit pulvinar fringilla. Vestibulum et urna quis sapien gravida ultricies eget quis ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam elit magna, aliquet at ex fermentum, volutpat congue augue. Vivamus vel ultrices orci, vitae scelerisque tellus. Fusce cursus vitae ligula et mollis. Curabitur diam enim, rutrum vitae vestibulum nec, dapibus at elit. Vestibulum et mi eu tellus viverra blandit lacinia vel dolor.

Nunc faucibus sagittis sapien, sit amet accumsan sem efficitur a. Sed urna purus, bibendum sed lorem in, finibus consectetur ante. Curabitur ut eros est. Curabitur placerat faucibus ante in porta. Pellentesque sed justo vitae orci euismod mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam accumsan magna vitae purus condimentum, in tincidunt odio eleifend. Suspendisse varius cursus tortor ut faucibus. Curabitur in justo in metus finibus commodo pretium quis velit. Donec ultricies, tortor non suscipit blandit, metus felis porta massa, vitae maximus nibh mi sit amet dui. Fusce vitae risus euismod augue lacinia vestibulum nec sit amet enim. Duis sit amet dignissim neque. Sed euismod vulputate dolor, eu convallis odio vehicula ac. Donec dolor sapien, commodo in hendrerit quis, gravida non felis. Nullam in mauris ac mi faucibus vehicula.

Donec consequat egestas laoreet. Suspendisse efficitur in leo sit amet ultrices. Fusce pharetra mauris erat. Ut in metus accumsan enim congue scelerisque quis at justo. Curabitur hendrerit ut dui quis lobortis. Aliquam ultricies hendrerit volutpat. Vivamus magna nisl, tincidunt at nisi eu, vestibulum finibus orci. Proin sodales iaculis mattis. Nulla diam enim, interdum et consectetur et, tempus eleifend nunc. Nam nisl turpis, condimentum quis erat vitae, posuere aliquet leo.

Proin at iaculis tortor, ac eleifend ipsum. Aliquam tincidunt placerat varius. Sed mattis eu tellus a rutrum. Maecenas finibus volutpat arcu, non pretium lacus tempor quis. Morbi efficitur urna a auctor tristique. Proin volutpat porta ante eget venenatis. Etiam pretium quis urna non vestibulum. Aliquam erat volutpat. Morbi suscipit luctus justo, in consequat nibh hendrerit at. Suspendisse potenti. Morbi tincidunt libero sed lorem blandit, quis tincidunt tellus pellentesque. Aenean vulputate purus sed neque sollicitudin tristique sed a metus. In elementum tellus id velit convallis, non sollicitudin risus sagittis. Etiam vulputate sollicitudin tellus viverra tristique. In molestie posuere dolor eu vulputate. Mauris hendrerit ultricies metus, a tincidunt purus.

Integer quis mollis neque, ut suscipit orci. Sed ornare efficitur fermentum. Sed at fringilla nisl. Suspendisse potenti. Duis vitae sem non urna laoreet vestibulum ac pretium dolor. Donec non turpis quis dui rutrum euismod sed vitae dui. In eget vehicula nisl. Duis ac nisi ac arcu ullamcorper pulvinar non sed lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mauris diam, maximus quis consequat ut, vulputate vitae urna. Mauris et vehicula nisi, sed eleifend libero.

Ut id risus lectus. Sed eget lectus nec enim porttitor ultricies. Sed eu porttitor neque. Cras vestibulum sem metus, et pretium lectus vestibulum vel. Aenean sed ligula at velit viverra placerat. Curabitur ullamcorper aliquam metus. Integer posuere, lorem a scelerisque luctus, lectus tellus posuere sem, eget imperdiet odio tellus vitae tellus. Proin posuere turpis vel lacus malesuada, sed ornare risus vulputate. Phasellus id metus elit. Nulla sed nunc sed est placerat sollicitudin et vitae eros. Quisque vel facilisis velit.

Praesent nibh purus, tristique vitae felis non, rutrum consequat lacus. Etiam dictum eleifend ullamcorper. Morbi in massa sed sem suscipit fringilla. Ut nisi ante, porta id dignissim in, bibendum nec mauris. Pellentesque semper porta purus, a scelerisque quam. Nulla iaculis erat tellus, at pretium nisl venenatis et. Morbi placerat massa facilisis nisl gravida, et vulputate neque sagittis. Sed sed tempus augue. Fusce bibendum, mi nec porta congue, quam risus pretium elit, ut efficitur lectus libero et libero. Nulla ut commodo eros. Quisque at ex porta, consequat mauris a, pharetra arcu. Vestibulum dui nibh, dictum egestas turpis sed, rutrum volutpat ante. Proin non imperdiet dolor. Nullam enim purus, suscipit nec velit in, hendrerit rhoncus velit.

Vestibulum pulvinar turpis id est vulputate, quis dictum dui dictum. Aliquam vitae nisl a enim porttitor rhoncus. Integer at accumsan felis. Donec vel sagittis libero. Suspendisse rhoncus ligula imperdiet vestibulum bibendum. Praesent placerat imperdiet ligula vitae pulvinar. Cras massa nibh, efficitur id elementum non, placerat vel velit. Cras porttitor mauris nec orci commodo lacinia. Nunc non nisi malesuada, commodo arcu id, posuere enim. Suspendisse sem arcu, cursus at suscipit quis, fermentum non nulla. Sed mollis imperdiet dui eu egestas. Proin non consequat justo. Nam id turpis sit amet arcu porttitor porttitor. Praesent sed accumsan magna. Aliquam erat volutpat.
:::
</details>

<details open>
<summary class="item-thumbnail toggle image-2">

![Haruni Shawls](https://vboyce.github.io/assets/multi-haruni-2.jpg){.thumbnail}

</summary>
::: {.item-description .description-2}
Three shawls, all from the same pattern, Haruni from Ravelry.
The green one was the first lace piece I ever made (maybe 2015?).
The multicolor and blue shawls were made in the next couple years.
I highly recommend this pattern as an introduction to lace knitting;
it has verbal description-items of the overall idea of the pattern,
and both charted and line-by-line instructions.
(I used the charts and ignored the line-by-line,
but the comparison was useful for making sure I understood how to read the charts.)
:::
</details>

<details>
<summary class="item-thumbnail toggle image-3">

![Owl Shawl](https://vboyce.github.io/assets/owl-1.jpg){.thumbnail}

</summary>
::: {.item-description .description-3}
Large shawl knit in a heavier yarn.
Pattern is Into The Woods Owl from Ravelry.
:::
</details>

<details>
<summary class="item-thumbnail toggle image-4">

![Red Shawlette](https://vboyce.github.io/assets/red-1.jpg){.thumbnail}

</summary>
::: {.item-description .description-4}
Made Spring 2017.
Pattern is Willow from New Vintage Lace by Andrea Jurgrau.
:::
</details>

<details>
<summary class="item-thumbnail toggle image-5">

![Large Blue Shawl](https://vboyce.github.io/assets/blue-round-1.jpg){.thumbnail}

</summary>
::: {.item-description .description-5}
Made Summer 2017.
Pattern is Coeur d’Amour from New Vintage Lace by Andrea Jurgrau.
:::
</details>

<details>
<summary class="item-thumbnail toggle image-6">

![Green Rectangular Shawl](https://vboyce.github.io/assets/green-rec.jpg){.thumbnail}

</summary>
::: {.item-description .description-6}
Pattern is Print O’the Wave Stole from Ravelry.
Made Winter 2017-2018.
:::
</details>

<details>
<summary class="item-thumbnail toggle image-7">

![Large Red Shawl](https://okeducationtruths.files.wordpress.com/2016/09/not-pictured.png){.thumbnail}

</summary>
::: {.item-description .description-7}
(not pictured) Made Summer 2018.
Pattern is Town Square Shawl from New Lace Knitting by Rosemary Hill.
:::
</details>

:::::

::::: {.details #details}
Click on a project to have more details show up here!
:::::

:::::::