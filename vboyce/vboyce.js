<script type="text/javascript">
function setup() {
  var toggles = Array.from(document.getElementsByClassName('toggle'));
  toggles.forEach(function(obj, idx) {
    obj.addEventListener('click', function(event) {
      var classes = Array.from(obj.classList);
      const regex = new RegExp('image-\\b');
      const matchedClasses = classes.filter((str) => regex.test(str));
      var imageNum = matchedClasses[0].split('-')[1];
      var toShow = Array.from(document.getElementsByClassName('description-' + imageNum));
      var details = document.getElementById('details');
      
      toShow.forEach(function(obj, idx) {
        details.innerHTML = obj.innerHTML;
      });
      
      // var visible = Array.from(document.getElementsByClassName('visible-description'));
      // 
      // toShow.forEach(function(obj, idx) {
      //   if (!visible.includes(obj)) {
      //     obj.classList.add("visible-description");
      //   } else {
      //     obj.classList.remove("visible-description");
      //   }
      // });
    });
  });
}
window.onload = setup;
</script>