---
pagetitle: Pi Fisher
subtitle: 4∑(-1)^n÷(2n+1) 🚣🎣
lang: en-us
...


<header class="heading-container">
::: {.heading-title}
# Pi Fisher # {.title}
[4∑(-1)^n÷(2n+1) 🚣🎣\
[🧔🏻@🚣🎣.ml](mailto:🧔🏻@🚣🎣.ml)]{.subtitle}
:::

::: {.heading-image}
![photo of Pi Fisher](HeadShot.png){#headshot}\
:::
</header>

---

* See my coding projects on [GitLab](https://www.gitlab.com/3geek14)!
* Check out my knitting on [Ravelry](https://www.ravelry.com/projects/3geek14)!
* The [Realms Omnibus](https://realms.🚣🎣.ml/Omnibus.html) is the rulebook for
  the LARP I play.

---

I'm a software engineer at Google, and I'm a member of the [Alphabet Workers
Union](https://alphabetworkersunion.org/). I enjoy many crafts, particularly
knitting, chainmail, and origami.